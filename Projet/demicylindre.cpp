#include "demicylindre.h"

#include <QColor>
#include <cmath>

DemiCylindre::DemiCylindre(QVector3D origin, int facesNb, float radius, float length, float thick, float red, float green, float blue)
    : m_origin(origin), m_facesNb(facesNb), m_radius(radius), m_length(length), m_thick(thick), m_red(red), m_green(green), m_blue(blue)
{}

void DemiCylindre::makeGLObjects()
{
    QString imageName = QString(":/metal1.png");
    QImage image(imageName);
    if(image.isNull()){
        qDebug() << "Problème de chargement de l'image " << imageName;
    }
    m_texture = new QOpenGLTexture(image);

    float angleR = (2.f * M_PI) /(m_facesNb*2);
    float angleD = 360.f/(m_facesNb*2);

    QVector<GLfloat> vertData;
    vertData.reserve(m_facesNb*(6*(3+3+2+3)+8*(3+3+2+3))+2*8*(3+3+2+3));

    auto pushV = [&vertData](const QVector3D& v) {
        for (int i = 0; i < 3; ++i) {
            vertData << v[i];
        }
    };

    auto pushC = [&vertData](float r, float g, float b) {
        vertData << r << g << b;
    };

    auto pushT = [&vertData](float x, float y) {
        vertData << x << y;
    };

    QVector3D A(m_origin),
            _A(m_origin.x(), m_origin.y(), m_origin.z() - m_thick),
            C(m_origin.x(), m_origin.y() - m_radius,m_origin.z()),
            _C(m_origin.x(), m_origin.y() - (m_radius - m_thick), m_origin.z() - m_thick),
            D(m_origin.x(), m_origin.y() - m_radius, m_origin.z() - m_length),
            _D(m_origin.x(), m_origin.y() - (m_radius - m_thick), m_origin.z() - m_length),
            E(m_origin.x() + std::sin(angleR) * m_radius, m_origin.y() -  std::cos(angleR) * m_radius, m_origin.z() - m_length),
            _E(m_origin.x() + std::sin(angleR) * (m_radius - m_thick), m_origin.y() -  std::cos(angleR) * (m_radius - m_thick), m_origin.z() - m_length),
            F(m_origin.x() + std::sin(angleR) * m_radius, m_origin.y() - std::cos(angleR) * m_radius, m_origin.z()),
            _F(m_origin.x() + std::sin(angleR) * (m_radius - m_thick), m_origin.y() - std::cos(angleR) * (m_radius - m_thick), m_origin.z() - m_thick);

    QMatrix4x4 matrix;
    QVector3D normalACF = QVector3D::normal(A, C, F);
    QVector3D _normalAFC = QVector3D::normal(_A, _F, _C);

    float xTex, nextXTex, yTex, nextYTex;

    for (int face = 0; face < m_facesNb; ++face) {

        xTex = 0.5 + 0.5 * std::sin(face * M_PI / m_facesNb),
        nextXTex = 0.5 + 0.5 * std::sin((face+1) * M_PI / m_facesNb),
        yTex = 0.5 - 0.5 * std::cos(face * M_PI / m_facesNb),
        nextYTex = 0.5 - 0.5 * std::cos((face+1) * M_PI / m_facesNb);

        pushV(matrix * A); pushC(m_red, m_green, m_green); pushT(0.5, 0.5); pushV(matrix * normalACF);
        pushV(matrix * C); pushC(m_red, m_green, m_blue); pushT(xTex, yTex); pushV(matrix * normalACF);
        pushV(matrix * F); pushC(m_red, m_green, m_blue); pushT(nextXTex, nextYTex); pushV(matrix * normalACF);
        pushV(matrix * _A); pushC(m_red, m_green, m_blue); pushT(0.5, 0.5); pushV(matrix * _normalAFC);
        pushV(matrix * _C); pushC(m_red, m_green, m_blue); pushT(xTex, yTex); pushV(matrix * _normalAFC);
        pushV(matrix * _F); pushC(m_red, m_green, m_blue); pushT(nextXTex, nextYTex); pushV(matrix * _normalAFC);

        matrix.rotate(angleD, 0.f, 0.f, 1.f);
    }

    matrix.setToIdentity();

    QMatrix4x4 newMatrix = matrix;
    newMatrix.rotate(angleD, 0.f, 0.f, 1.f);

    QVector3D normalAAC = QVector3D::normal(A, _A, C);
    QVector3D normalACA = QVector3D::normal(A, C, _A);

    QVector3D normalCCD = QVector3D::normal(C, _C, D);
    QVector3D normalCDC = QVector3D::normal(C, D, _C);

    pushV(matrix * A); pushC(m_red, m_green, m_blue); pushT(0, 0); pushV(matrix * normalAAC);
    pushV(matrix * _A); pushC(m_red, m_green, m_blue); pushT(0, 1); pushV(matrix * normalAAC);
    pushV(matrix * _C); pushC(m_red, m_green, m_blue); pushT(1, 1); pushV(matrix * normalAAC);
    pushV(matrix * C); pushC(m_red, m_green, m_blue); pushT(1, 0); pushV(matrix * normalAAC);

    pushV(matrix * C); pushC(m_red, m_green, m_blue); pushT(0, 0); pushV(matrix * normalCCD);
    pushV(matrix * _C); pushC(m_red, m_green, m_blue); pushT(0, 1); pushV(matrix * normalCCD);
    pushV(matrix * _D); pushC(m_red, m_green, m_blue); pushT(1, 1); pushV(matrix * normalCCD);
    pushV(matrix * D); pushC(m_red, m_green, m_blue); pushT(1, 0); pushV(matrix * normalCCD);

    QVector3D normalEFD = QVector3D::normal(E, F, D),
            nextE = newMatrix * E,
            nextF = newMatrix * F,
            nextD = newMatrix * D,
            nextNormalEFD = QVector3D::normal(nextE,nextF,nextD),
            oldNormalMeanEFD = normalEFD,
            normalMeanEFD = (normalEFD + nextNormalEFD) / 2.0;

    QVector3D _normalEDF = QVector3D::normal(_E, _D, _F),
            _nextE = newMatrix * _E,
            _nextF = newMatrix * _F,
            _nextD = newMatrix * _D,
            _nextNormalEDF = QVector3D::normal(_nextE,_nextD,_nextF),
            _oldNormalMeanEDF = _normalEDF,
            _normalMeanEDF = (_normalEDF + _nextNormalEDF) / 2.0;

    for (int face = 0; face < m_facesNb; ++face) {

        xTex = face / m_facesNb;
        nextXTex = (face + 1) / m_facesNb;

        pushV(matrix * C); pushC(m_red, m_green, m_blue); pushT(xTex, 0); pushV(oldNormalMeanEFD);
        pushV(matrix * D); pushC(m_red, m_green, m_blue); pushT(xTex, 1); pushV(oldNormalMeanEFD);
        pushV(matrix * E); pushC(m_red, m_green, m_blue); pushT(nextXTex, 1); pushV(normalMeanEFD);
        pushV(matrix * F); pushC(m_red, m_green, m_blue); pushT(nextXTex, 0); pushV(normalMeanEFD);
        pushV(matrix * _C); pushC(m_red, m_green, m_blue); pushT(xTex, 0); pushV(_oldNormalMeanEDF);
        pushV(matrix * _D); pushC(m_red, m_green, m_blue); pushT(xTex, 1); pushV(_oldNormalMeanEDF);
        pushV(matrix * _E); pushC(m_red, m_green, m_blue); pushT(nextXTex, 1); pushV(_normalMeanEDF);
        pushV(matrix * _F); pushC(m_red, m_green, m_blue); pushT(nextXTex, 0); pushV(_normalMeanEDF);

        matrix.rotate(angleD, 0.f, 0.f, 1.f);
        newMatrix = matrix;
        newMatrix.rotate(angleD, 0.f, 0.f, 1.f);

        normalEFD = nextNormalEFD;
        nextE = newMatrix * E;
        nextF = newMatrix * F;
        nextD = newMatrix * D;
        nextNormalEFD = QVector3D::normal(nextE,nextF,nextD);
        oldNormalMeanEFD = normalMeanEFD;
        normalMeanEFD = (normalEFD + nextNormalEFD) / 2.0;

        _normalEDF = _nextNormalEDF;
        _nextE = newMatrix * _E;
        _nextF = newMatrix * _F;
        _nextD = newMatrix * _D;
        _nextNormalEDF = QVector3D::normal(_nextE,_nextD,_nextF);
        _oldNormalMeanEDF = _normalMeanEDF;
        _normalMeanEDF = (_normalEDF + _nextNormalEDF) / 2.0;
    }

    pushV(matrix * A); pushC(m_red, m_green, m_blue); pushT(0, 0); pushV(matrix * normalACA);
    pushV(matrix * _A); pushC(m_red, m_green, m_blue); pushT(0, 1); pushV(matrix * normalACA);
    pushV(matrix * _C); pushC(m_red, m_green, m_blue); pushT(1, 1); pushV(matrix * normalACA);
    pushV(matrix * C); pushC(m_red, m_green, m_blue); pushT(1, 0); pushV(matrix * normalACA);

    pushV(matrix * C); pushC(m_red, m_green, m_blue); pushT(0, 0); pushV(matrix * normalCDC);
    pushV(matrix * _C); pushC(m_red, m_green, m_blue); pushT(0, 1); pushV(matrix * normalCDC);
    pushV(matrix * _D); pushC(m_red, m_green, m_blue); pushT(1, 1); pushV(matrix * normalCDC);
    pushV(matrix * D); pushC(m_red, m_green, m_blue); pushT(1, 0); pushV(matrix * normalCDC);

    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(), vertData.count() * sizeof(GLfloat));
}

void DemiCylindre::tearGLObjects()
{
    m_vbo.destroy();
    delete m_texture;
}

void DemiCylindre::draw(QMatrix4x4 projMatrix, QMatrix4x4 mvMatrix, QMatrix3x3 norMatrix, QOpenGLShaderProgram* program,
                        int posAttr, int colAttr, int texAttr, int norAttr, int projUniform, int mvUniform, int norUniform)
{
    initializeOpenGLFunctions();
    m_vbo.bind();

    program->setUniformValue(projUniform, projMatrix);
    program->setUniformValue(mvUniform, mvMatrix);
    program->setUniformValue(norUniform, norMatrix);

    program->setAttributeBuffer(posAttr, GL_FLOAT, 0, 3, 11 * sizeof(GLfloat));
    program->setAttributeBuffer(colAttr, GL_FLOAT, 3 * sizeof(GLfloat), 3, 11 * sizeof(GLfloat));
    program->setAttributeBuffer(texAttr, GL_FLOAT, 6 * sizeof(GLfloat), 2, 11 * sizeof(GLfloat));
    program->setAttributeBuffer(norAttr, GL_FLOAT, 8 * sizeof(GLfloat), 3, 11 * sizeof(GLfloat));

    program->enableAttributeArray(posAttr);
    program->enableAttributeArray(colAttr);
    program->enableAttributeArray(texAttr);
    program->enableAttributeArray(norAttr);

    m_texture->bind();
    glDrawArrays(GL_TRIANGLES, 0, m_facesNb * 6);
    glDrawArrays(GL_QUADS, m_facesNb * 6, 8);
    glDrawArrays(GL_QUADS, m_facesNb * 6 + 8, m_facesNb * 8);
    glDrawArrays(GL_QUADS, m_facesNb * 6 + 8 + m_facesNb * 8, 8);
    m_texture->release();

    program->disableAttributeArray(posAttr);
    program->disableAttributeArray(colAttr);
    program->disableAttributeArray(texAttr);
    program->disableAttributeArray(norAttr);
    m_vbo.release();
}
