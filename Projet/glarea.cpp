// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"

#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>
#include <cmath>

static const QString vertexShaderFile   = ":/vertex.glsl";
static const QString fragmentShaderFile = ":/fragment.glsl";


GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent),
    m_demiCylindreRadius(0.5),
    m_demiCylindreLength(2),
    m_demiCylindreThick(0.1),
    m_fixationLength(0.2),
    m_roueLength(0.1),
    m_distFixationPiston(2.5),
    m_distRoueFixation(m_demiCylindreRadius - m_demiCylindreThick),
    m_demiCylindre(QVector3D(0, 0, 0), 10, m_demiCylindreRadius, m_demiCylindreLength, m_demiCylindreThick, 1, 0.6, 0.6),
    m_piston(QVector3D(0, 0, 0), 20, m_demiCylindreRadius - m_demiCylindreThick, m_demiCylindreLength/3, 1, 1, 1),
    m_connexion(QVector3D(0, 0, 0), 10, m_demiCylindreRadius/10, m_distFixationPiston, 0.2, 0.5, 0.7),
    m_roue(QVector3D(0, 0, 0), 20, m_demiCylindreRadius, m_roueLength, 1, 1, 1),
    m_fixation(QVector3D(0, 0, 0), 10, m_demiCylindreRadius/5, m_fixationLength, 1, 1, 1)
{
    qDebug() << "init GLArea" ;

    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);  // nb de sample par pixels : suréchantillonnage por l'antialiasing, en décalant à chaque fois le sommet
                        // cf https://www.khronos.org/opengl/wiki/Multisampling et https://stackoverflow.com/a/14474260
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    m_timer = new QTimer(this);
    m_timer->setInterval(25);  // msec
    connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    connect (this, SIGNAL(radiusChanged(double)), this, SLOT(setRadius(double)));
}

GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    delete m_timer;

    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();

    m_demiCylindre.tearGLObjects();
    m_piston.tearGLObjects();
    m_connexion.tearGLObjects();
    m_roue.tearGLObjects();
    m_fixation.tearGLObjects();

    doneCurrent();
}


void GLArea::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);

    m_demiCylindre.makeGLObjects();
    m_piston.makeGLObjects();
    m_connexion.makeGLObjects();
    m_roue.makeGLObjects();
    m_fixation.makeGLObjects();

    // shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);
    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }

    // récupère identifiants de "variables" dans les shaders
    m_posAttr = m_program->attributeLocation("posAttr");
    m_colAttr = m_program->attributeLocation("colAttr");
    m_texAttr = m_program->attributeLocation("texAttr");
    m_norAttr = m_program->attributeLocation("norAttr");
    m_projUniform = m_program->uniformLocation("projMatrix");
    m_mvUniform = m_program->uniformLocation("mvMatrix");
    m_norUniform = m_program->uniformLocation("norMatrix");

    m_program->setUniformValue("texture", 0);
}

void GLArea::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;

    // C'est fait par défaut
    glViewport(0, 0, w, h);

    m_ratio = (double) w / h;
    // doProjection();
}

void GLArea::paintGL()
{
    //qDebug() << __FUNCTION__ ;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_program->bind(); // active le shader program

    QMatrix4x4 camMatrix;
    QMatrix4x4 projMatrix;
    QMatrix4x4 worldMatrix;

    GLfloat hr = m_radius, wr = hr * m_ratio;
    projMatrix.frustum(-wr, wr, -hr, hr, 1.0, 10.0);

    camMatrix.translate(0, 0, -3.0);
    camMatrix.rotate(90, 0, 1, 0);
    camMatrix.scale(0.3);

    // Rotation de la scène pour l'animation
    camMatrix.rotate(m_angle, 0, 1, 0);

    float alpha1 = m_anim * 2 * M_PI;
    float alpha2 = alpha1 + M_PI/2;

    QVector3D roue1(0, 1, 0);
    QVector3D roue2(0, -1, 0);
    QVector3D demiCylindre1(-m_fixationLength/2, 1, 4);
    QVector3D demiCylindre2(m_fixationLength/2 + m_roueLength, 1, -4);
    QVector3D demiCylindre3(-m_fixationLength/2, -1, 4);
    QVector3D demiCylindre4(m_fixationLength/2 + m_roueLength, -1, -4);
    QVector3D fixation1 = roue1 + QVector3D(0, std::sin(alpha1) * m_distRoueFixation, std::cos(alpha1) * m_distRoueFixation);
    QVector3D fixation2 = roue1 + QVector3D(m_roueLength, -std::sin(alpha1) * m_distRoueFixation, -std::cos(alpha1) * m_distRoueFixation);
    QVector3D fixation3 = roue2 + QVector3D(0, std::sin(alpha2) * m_distRoueFixation, std::cos(alpha2) * m_distRoueFixation);
    QVector3D fixation4 = roue2 + QVector3D(m_roueLength, -std::sin(alpha2) * m_distRoueFixation, -std::cos(alpha2) * m_distRoueFixation);
    QVector3D projFixation1(-m_fixationLength/2, roue1.y(), fixation1.z());
    QVector3D projFixation2(m_fixationLength/2 + m_roueLength, roue1.y(), fixation2.z());
    QVector3D projFixation3(-m_fixationLength/2, roue2.y(), fixation3.z());
    QVector3D projFixation4(m_fixationLength/2 + m_roueLength, roue2.y(), fixation4.z());
    QVector3D piston1(-m_fixationLength/2, projFixation1.y(), projFixation1.z() + std::sqrt(std::pow(m_distFixationPiston, 2) + std::pow(m_distRoueFixation * std::sin(alpha1), 2)));
    QVector3D piston2(m_fixationLength/2 + m_roueLength, projFixation2.y(), projFixation2.z() - std::sqrt(std::pow(m_distFixationPiston, 2) - std::pow(m_distRoueFixation * std::sin(alpha1), 2)));
    QVector3D piston3(-m_fixationLength/2, projFixation3.y(), projFixation3.z() + std::sqrt(std::pow(m_distFixationPiston, 2) + std::pow(m_distRoueFixation * std::sin(alpha2), 2)));
    QVector3D piston4(m_fixationLength/2 + m_roueLength, projFixation4.y(), projFixation4.z() - std::sqrt(std::pow(m_distFixationPiston, 2) - std::pow(m_distRoueFixation * std::sin(alpha2), 2)));

    float beta1 = std::atan((fixation1.y() - piston1.y()) / (piston1.z() - fixation1.z()));
    float beta2 = std::atan((fixation2.y() - piston2.y()) / (piston2.z() - fixation2.z()));
    float beta3 = std::atan((fixation3.y() - piston3.y()) / (piston3.z() - fixation3.z()));
    float beta4 = std::atan((fixation4.y() - piston4.y()) / (piston4.z() - fixation4.z()));


    // -------------------Roues(Vilebrequins)-------------------
    { // Haut
        auto m = worldMatrix;
        m.translate(roue1);
        m.rotate(270, 0, 1, 0);
        m.rotate(m_anim * 360, 0, 0, 1);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_roue);
    }
    { // Bas
        auto m = worldMatrix;
        m.translate(roue2);
        m.rotate(270, 0, 1, 0);
        m.rotate(m_anim * 360, 0, 0, 1);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_roue);
    }

    // -------------------Demi-Cylindres-------------------
    { // Haut droite
        auto m = worldMatrix;
        m.translate(demiCylindre1);
        auto n = m.normalMatrix();
        draw_demiCylindre(projMatrix, camMatrix*m, n, m_demiCylindre);
    }
    { // Haut gauche
        auto m = worldMatrix;
        m.translate(demiCylindre2);
        m.rotate(180, 1, 0, 0);
        auto n = m.normalMatrix();
        draw_demiCylindre(projMatrix, camMatrix*m, n, m_demiCylindre);
    }
    { // Bas droite
        auto m = worldMatrix;
        m.translate(demiCylindre3);
        auto n = m.normalMatrix();
        draw_demiCylindre(projMatrix, camMatrix*m, n, m_demiCylindre);
    }
    { // Bas gauche
        auto m = worldMatrix;
        m.translate(demiCylindre4);
        m.rotate(180, 1, 0, 0);
        auto n = m.normalMatrix();
        draw_demiCylindre(projMatrix, camMatrix*m, n, m_demiCylindre);
    }

    // -------------------Pistons-------------------
    { // Haut droite
        auto m = worldMatrix;
        m.translate(piston1);
        m.rotate(180, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_piston);
    }
    { // Haut gauche
        auto m = worldMatrix;
        m.translate(piston2);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_piston);
    }
    { // Bas droite
        auto m = worldMatrix;
        m.translate(piston3);
        m.rotate(180, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_piston);
    }
    { // Bas gauche
        auto m = worldMatrix;
        m.translate(piston4);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_piston);
    }

    // -------------------Fixations-------------------
    { // Haut droite
        auto m = worldMatrix;
        m.translate(fixation1);
        m.rotate(90, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_fixation);
    }
    { // Haut gauche
        auto m = worldMatrix;
        m.translate(fixation2);
        m.rotate(270, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_fixation);
    }
    { // Bas droite
        auto m = worldMatrix;
        m.translate(fixation3);
        m.rotate(90, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_fixation);
    }
    { // Bas gauche
        auto m = worldMatrix;
        m.translate(fixation4);
        m.rotate(270, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_fixation);
    }

    // -------------------Connexions-------------------
    { // Haut droite
        auto m = worldMatrix;
        m.translate(piston1);
        m.rotate(beta1 / M_PI * 180, 1, 0, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_connexion);
    }
    { // Haut gauche
        auto m = worldMatrix;
        m.translate(piston2);
        m.rotate(beta2 / M_PI * 180, 1, 0, 0);
        m.rotate(180, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_connexion);
    }
    { // Bas droite
        auto m = worldMatrix;
        m.translate(piston3);
        m.rotate(beta3 / M_PI * 180, 1, 0, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_connexion);
    }
    { // Bas gauche
        auto m = worldMatrix;
        m.translate(piston4);
        m.rotate(beta4 / M_PI * 180, 1, 0, 0);
        m.rotate(180, 0, 1, 0);
        auto n = m.normalMatrix();
        draw_cylindre(projMatrix, camMatrix*m, n, m_connexion);
    }

    m_program->release();
}

void GLArea::keyPressEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();

    switch(ev->key()) {
        case Qt::Key_Space :
            m_angle += 1;
            if (m_angle >= 360) m_angle -= 360;
            update();
            break;
        case Qt::Key_D :
            m_timer->start();
            emit speedChanged(m_timer->interval());
            break;
        case Qt::Key_S :
            m_timer->stop();
            emit speedChanged(0);
            break;
        case Qt::Key_A :
            if(m_timer->isActive()){
                if(ev->modifiers() == Qt::ShiftModifier){
                    if(m_timer->interval() > 5){
                        m_timer->setInterval(m_timer->interval() - 5);
                    }
                }
                else if(m_timer->interval() < 50){
                    m_timer->setInterval(m_timer->interval() + 5);
                }
                emit speedChanged(m_timer->interval());
                qDebug() << m_timer->interval();
            }
            break;
        case Qt::Key_R :
            if (ev->text() == "r")
                 setRadius(m_radius-0.05);
            else setRadius(m_radius+0.05);
            break;
    }
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}

void GLArea::mousePressEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y();
}

void GLArea::draw_demiCylindre(QMatrix4x4 projMatrix, QMatrix4x4 mvMatrix, QMatrix3x3 norMatrix, DemiCylindre &demiCylindre)
{
    demiCylindre.draw(projMatrix, mvMatrix, norMatrix, m_program, m_posAttr, m_colAttr, m_texAttr, m_norAttr, m_projUniform, m_mvUniform, m_norUniform);
}

void GLArea::draw_cylindre(QMatrix4x4 projMatrix, QMatrix4x4 mvMatrix, QMatrix3x3 norMatrix, Cylindre &cylindre)
{
    cylindre.draw(projMatrix, mvMatrix, norMatrix, m_program, m_posAttr, m_colAttr, m_texAttr, m_norAttr, m_projUniform, m_mvUniform, m_norUniform);
}

void GLArea::onTimeout()
{
    //qDebug() << __FUNCTION__ ;
    m_anim += 0.03;
    if (m_anim > 1) m_anim = 0;
    update();
}

void GLArea::setRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    if (radius != m_radius && radius > 0.01 && radius <= 10) {
        m_radius = radius;
        qDebug() << "  emit radiusChanged()";
        emit radiusChanged(radius);
        update();
    }
}




