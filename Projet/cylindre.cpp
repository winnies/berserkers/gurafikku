#include "cylindre.h"

#include <QColor>
#include <cmath>

Cylindre::Cylindre(QVector3D origin, int facesNb, float radius, float length, float red, float green, float blue)
    : m_origin(origin), m_facesNb(facesNb), m_radius(radius), m_length(length), m_red(red), m_green(green), m_blue(blue)
{}

void Cylindre::makeGLObjects()
{
    QString imageName = QString(":/metal1.png");
    QImage image(imageName);
    if(image.isNull()){
        qDebug() << "Problème de chargement de l'image " << imageName;
    }
    m_texture = new QOpenGLTexture(image);

    float angleR = (2.f * M_PI) /m_facesNb;
    float angleD = 360.f/m_facesNb;

    QVector<GLfloat> vertData;
    vertData.reserve(m_facesNb*(6*(3+3+2+3)+4*(3+3+2+3)));

    auto pushV = [&vertData](const QVector3D& v) {
        for (int i = 0; i < 3; ++i) {
            vertData << v[i];
        }
    };

    auto pushC = [&vertData](float r, float g, float b) {
        vertData << r << g << b;
    };

    auto pushT = [&vertData](float x, float y) {
        vertData << x << y;
    };

    QVector3D A(m_origin),
            B(m_origin.x(), m_origin.y(), -m_length),
            C(m_origin.x(), m_origin.y() - m_radius, m_origin.z()),
            D(m_origin.x(), m_origin.y() - m_radius, m_origin.z() - m_length),
            E(m_origin.x() + std::sin(angleR) * m_radius, m_origin.y() -  std::cos(angleR) * m_radius, m_origin.z() - m_length),
            F(m_origin.x() + std::sin(angleR) * m_radius, m_origin.y() - std::cos(angleR) * m_radius, m_origin.z());

    QMatrix4x4 matrix;
    QVector3D normalACF = QVector3D::normal(A, C, F);
    QVector3D normalBED = QVector3D::normal(B, E, D);

    float xTex, nextXTex, yTex, nextYTex;

    for (int face = 0; face < m_facesNb; ++face) {

        xTex = 0.5 + 0.5 * std::sin(face * M_PI * 2 / m_facesNb),
        nextXTex = 0.5 + 0.5 * std::sin((face+1) * M_PI * 2 / m_facesNb),
        yTex = 0.5 - 0.5 * std::cos(face * M_PI * 2 / m_facesNb),
        nextYTex = 0.5 - 0.5 * std::cos((face+1) * M_PI * 2 / m_facesNb);

        pushV(matrix * A); pushC(m_red, m_green, m_green); pushT(0.5, 0.5); pushV(matrix * normalACF);
        pushV(matrix * C); pushC(m_red, m_green, m_blue); pushT(xTex, yTex); pushV(matrix * normalACF);
        pushV(matrix * F); pushC(m_red, m_green, m_blue); pushT(nextXTex, nextYTex); pushV(matrix * normalACF);
        pushV(matrix * B); pushC(m_red, m_green, m_green); pushT(0.5, 0.5); pushV(matrix * normalBED);
        pushV(matrix * D); pushC(m_red, m_green, m_blue); pushT(xTex, yTex); pushV(matrix * normalBED);
        pushV(matrix * E); pushC(m_red, m_green, m_blue); pushT(nextXTex, nextYTex); pushV(matrix * normalBED);

        matrix.rotate(angleD, 0.f, 0.f, 1.f);
    }

    matrix.setToIdentity();

    QMatrix4x4 newMatrix = matrix;
    newMatrix.rotate(angleD, 0.f, 0.f, 1.f);

    QVector3D normalEFD = QVector3D::normal(E, F, D),
            nextE = newMatrix * E,
            nextF = newMatrix * F,
            nextD = newMatrix * D,
            nextNormalEFD = QVector3D::normal(nextE,nextF,nextD),
            oldNormalMeanEFD = normalEFD,
            normalMeanEFD = (normalEFD + nextNormalEFD) / 2.0;

    for (int face = 0; face < m_facesNb; ++face) {

        xTex = face / m_facesNb;
        nextXTex = (face + 1) / m_facesNb;

        pushV(matrix * C); pushC(m_red*0.8f, m_green*0.8f, m_blue*0.8f); pushT(xTex, 0); pushV(oldNormalMeanEFD);
        pushV(matrix * D); pushC(m_red*0.8f, m_green*0.8f, m_blue*0.8f); pushT(xTex, 1); pushV(oldNormalMeanEFD);
        pushV(matrix * E); pushC(m_red*0.8f, m_green*0.8f, m_blue*0.8f); pushT(nextXTex, 1); pushV(normalMeanEFD);
        pushV(matrix * F); pushC(m_red*0.8f, m_green*0.8f, m_blue*0.8f); pushT(nextXTex, 0); pushV(normalMeanEFD);

        matrix.rotate(angleD, 0.f, 0.f, 1.f);
        newMatrix = matrix;
        newMatrix.rotate(angleD, 0.f, 0.f, 1.f);

        normalEFD = nextNormalEFD;
        nextE = newMatrix * E;
        nextF = newMatrix * F;
        nextD = newMatrix * D;
        nextNormalEFD = QVector3D::normal(nextE,nextF,nextD);
        oldNormalMeanEFD = normalMeanEFD;
        normalMeanEFD = (normalEFD + nextNormalEFD) / 2.0;
    }

    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(), vertData.count() * sizeof(GLfloat));
}

void Cylindre::tearGLObjects()
{
    m_vbo.destroy();
    delete m_texture;
}

void Cylindre::draw(QMatrix4x4 projMatrix, QMatrix4x4 mvMatrix, QMatrix3x3 norMatrix, QOpenGLShaderProgram* program,
                    int posAttr, int colAttr, int texAttr, int norAttr, int projUniform, int mvUniform, int norUniform)
{
    initializeOpenGLFunctions();
    m_vbo.bind();

    program->setUniformValue(projUniform, projMatrix);
    program->setUniformValue(mvUniform, mvMatrix);
    program->setUniformValue(norUniform, norMatrix);

    program->setAttributeBuffer(posAttr, GL_FLOAT, 0, 3, 11 * sizeof(GLfloat));
    program->setAttributeBuffer(colAttr, GL_FLOAT, 3 * sizeof(GLfloat), 3, 11 * sizeof(GLfloat));
    program->setAttributeBuffer(texAttr, GL_FLOAT, 6 * sizeof(GLfloat), 2, 11 * sizeof(GLfloat));
    program->setAttributeBuffer(norAttr, GL_FLOAT, 8 * sizeof(GLfloat), 3, 11 * sizeof(GLfloat));

    program->enableAttributeArray(posAttr);
    program->enableAttributeArray(colAttr);
    program->enableAttributeArray(texAttr);
    program->enableAttributeArray(norAttr);

    m_texture->bind();
    glDrawArrays(GL_TRIANGLES, 0, m_facesNb * 6);
    glDrawArrays(GL_QUADS, m_facesNb * 6, m_facesNb * 4);
    m_texture->release();

    program->disableAttributeArray(posAttr);
    program->disableAttributeArray(colAttr);
    program->disableAttributeArray(texAttr);
    program->disableAttributeArray(norAttr);
    m_vbo.release();
}
