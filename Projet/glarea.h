﻿// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef GLAREA_H
#define GLAREA_H

#include "cylindre.h"
#include "demicylindre.h"
#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = 0);
    ~GLArea();

public slots:
    void setRadius(double radius);

signals:  // On ne les implémente pas, elles seront générées par MOC ;
          // les paramètres seront passés aux slots connectés.
    void radiusChanged(double newRadius);
    void speedChanged(int speed);

protected slots:
    void onTimeout();

protected:
    void initializeGL() override;
    void doProjection();
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

    void draw_demiCylindre(QMatrix4x4 projMatrix, QMatrix4x4 mvMatrix, QMatrix3x3 norMatrix, DemiCylindre& demiCylindre);
    void draw_cylindre(QMatrix4x4 projMatrix, QMatrix4x4 mvMatrix, QMatrix3x3 norMatrix, Cylindre& cylindre);

private:
    double m_angle = 0;
    QTimer *m_timer = nullptr;
    double m_anim = 0;
    double m_radius = 0.5;
    double m_ratio = 1;

    QOpenGLShaderProgram *m_program;
    int m_posAttr;
    int m_colAttr;
    int m_texAttr;
    int m_norAttr;
    int m_projUniform;
    int m_mvUniform;
    int m_norUniform;

    float m_demiCylindreRadius;
    float m_demiCylindreLength;
    float m_demiCylindreThick;
    float m_fixationLength;
    float m_roueLength;
    float m_distFixationPiston;
    float m_distRoueFixation;

    DemiCylindre m_demiCylindre;
    Cylindre m_piston;
    Cylindre m_connexion;
    Cylindre m_roue;
    Cylindre m_fixation;
};

#endif // GLAREA_H
