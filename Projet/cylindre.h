#ifndef CYLINDRE_H
#define CYLINDRE_H

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

class Cylindre : protected QOpenGLFunctions {
public:
    Cylindre(QVector3D origin, int facesNb, float radius, float length, float red, float green, float blue);

    void makeGLObjects();
    void tearGLObjects();

    void draw(QMatrix4x4 projMatrix, QMatrix4x4 mvMatrix, QMatrix3x3 norMatrix, QOpenGLShaderProgram* program,
              int posAttr, int colAttr, int texAttr, int norAttr, int projUniform, int mvUniform, int norUniform);

private:
    QVector3D m_origin;
    float m_facesNb, m_radius, m_length, m_red, m_green, m_blue;
    QOpenGLBuffer m_vbo;
    QOpenGLTexture* m_texture;
};

#endif // CYLINDRE_H
