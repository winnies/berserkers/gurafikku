varying lowp vec4 col;
varying mediump vec4 texc;
varying highp vec3 nor;
uniform sampler2D texture;

void main() {

    vec3 light = normalize(vec3(-5.0, 0.0, 5.0));
    vec3 nor3 = normalize(nor);
    float cosTheta = clamp(dot(nor3, light), 0.3, 1.0);
    vec3 lightColor = vec3(1.0, 1.0, 1.0);
    gl_FragColor = vec4(lightColor * cosTheta, 1.0) * texture2D(texture, texc.st) * col;
}
