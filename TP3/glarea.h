// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef GLAREA_H
#define GLAREA_H

#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class Cylindre : protected QOpenGLFunctions {
public:
    Cylindre(float ep_cyl, float r_cyl, int nb_face, float coul_r, float coul_v, float coul_b);

    void draw(QMatrix4x4 matrix, QOpenGLShaderProgram *m_program, int m_posAttr, int m_colAttr, int m_matrixUniform);

private:
    int nb_face;
    float angleD;

    std::vector<QVector3D> triangle_vertices;
    std::vector<QVector3D> quad_vertices;
    std::vector<float> triangle_colors;
    std::vector<float> quad_colors;
};

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = 0);
    ~GLArea();

public slots:
    void setRadius(double radius);

signals:  // On ne les implémente pas, elles seront générées par MOC ;
          // les paramètres seront passés aux slots connectés.
    void radiusChanged(double newRadius);

protected slots:
    void onTimeout();

protected:
    void initializeGL() override;
    void doProjection();
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

    void dessiner_cylindre(const QMatrix4x4& matrix, Cylindre &cylindre);

private:
    double m_angle = 0;
    QTimer *m_timer = nullptr;
    double m_anim = 0;
    double m_radius = 0.5;
    double m_ratio = 1;

    // Pour utiliser les shaders
    QOpenGLShaderProgram *m_program;
    int m_posAttr;
    int m_colAttr;
    int m_matrixUniform;

    const float gh_norm = 1.f;
    const float hj_norm = 2.5f;

    Cylindre roue1, roue2, roue3, roue4, axe1, axe2, axe3, HJ, JK, piston;
};

#endif // GLAREA_H
