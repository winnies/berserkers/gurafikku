//// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>
#include <cmath>
#include <QTimer>
#include <QOpenGLShaderProgram>
#include <QKeyEvent>

#define M_PI 3.14159265358979323846f

static const QString vertexShaderFile   = ":/basic.vsh";
static const QString fragmentShaderFile = ":/basic.fsh";


GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent),
    roue1(0.5f, gh_norm + 0.2f, 20, 0.5f, 0.5f, 1.f),
    roue2(0.4f, 0.3f, 20, 1.f, 0.5f, 0.5f),
    roue3(0.5f, 0.4f, 20, 1.f, 0.5f, 0.5f),
    roue4(0.5f, 0.4f, 20, 1.f, 0.5f, 0.5f),
    axe1(1.1f, 0.1f, 20, 0.0f, 0.0f, 1.f),
    axe2(1.3f, 0.1f, 20, 0.0f, 0.0f, 1.f),
    axe3(1.4f, 0.1f, 20, 0.0f, 0.0f, 1.f),
    HJ(hj_norm, 0.1f, 20, 0.f, 1.f, 0.f),
    JK(hj_norm, 0.1f, 20, 0.f, 1.f, 0.f),
    piston(1.f, 0.4f, 20, 1.f, 0.f, 0.2f),
    cylindres{&roue1, &roue2, &roue3, &roue4, &axe1, &axe2, &axe3, &HJ, &JK, &piston}
{
    qDebug() << "init GLArea" ;

    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);  // nb de sample par pixels : suréchantillonnage por l'antialiasing, en décalant à chaque fois le sommet
                        // cf https://www.khronos.org/opengl/wiki/Multisampling et https://stackoverflow.com/a/14474260
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    m_timer = new QTimer(this);
    m_timer->setInterval(50);  // msec
    connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    connect (this, SIGNAL(radiusChanged(double)), this, SLOT(setRadius(double)));
}

GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    delete m_timer;

    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();

    // ici destructions de ressources GL
    for (Cylindre *c : cylindres) {
        c->tearGLObjects();
    }

    doneCurrent();
}

void GLArea::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);

    for (Cylindre *c : cylindres) {
        c->makeGLObjects();
    }

    // shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);
    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }

    // récupère identifiants de "variables" dans les shaders
    m_posAttr = m_program->attributeLocation("posAttr");
    m_colAttr = m_program->attributeLocation("colAttr");
    m_matrixUniform = m_program->uniformLocation("matrix");
}

void GLArea::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;

    // C'est fait par défaut
    glViewport(0, 0, w, h);

    m_ratio = (double) w / h;
    // doProjection();
}

void GLArea::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_program->bind(); // active le shader program

    QMatrix4x4 matrix;
    GLfloat hr = m_radius, wr = hr * m_ratio;            // = glFrustum
    matrix.frustum(-wr, wr, -hr, hr, 1.0, 5.0);
    //matrix.perspective(60.0f, m_ratio, 0.1f, 100.0f);  // = gluPerspective

    // Remplace gluLookAt (0, 0, 3.0, 0, 0, 0, 0, 1, 0);
    matrix.translate(0, 0, -3.0);

    // Rotation de la scène pour l'animation
    matrix.rotate(m_angle, 0, 1, 0);

    matrix.scale(0.3);

    float alphaR = m_anim * (2.f * M_PI);
    float alphaD = m_anim * 360.f;

    QVector3D G(1.f, 0.f, 0.f), K(-3.5f, 0.f, 0.f);
    QVector3D O = G + QVector3D(0.f, 0.f, -0.25f);
    QVector3D H = G + QVector3D(std::cos(alphaR), std::sin(alphaR), 0.f) * gh_norm;
    QVector3D I(H.x(), G.y(), 0.f);
    QVector3D J(I.x() - std::sqrt(std::pow(hj_norm, 2) - std::pow(gh_norm * std::sin(alphaR), 2)), I.y(), 0.f);

    float beta = std::atan(H.y() / std::abs(J.x() - H.x()));

    // Roue 1 (principale)
    {
        auto m = matrix;
        m.translate(O);
        m.rotate(alphaD, 0.f, 0.f, 1.f);
        dessiner_cylindre(m, roue1);
    }

    // Axe 1
    {
        auto m = matrix;
        m.translate(0.f, 0.f, -0.5f);
        m.translate(G);
        m.rotate(alphaD, 0.f, 0.f, 1.f);
        dessiner_cylindre(m, axe1);
    }

    // Axe 2
    {
        auto m = matrix;
        m.translate(H);
        dessiner_cylindre(m, axe2);
    }

    // Roue 2
    matrix.translate(0.f, 0.f, 0.4f);
    {
        auto m = matrix;
        m.translate(H);
        dessiner_cylindre(m, roue2);
    }

    // Roue 3
    {
        auto m = matrix;
        m.translate(J);
        dessiner_cylindre(m, roue3);
    }

    // Cylindre H - J
    {
        auto m = matrix;
        m.translate(J.x(), J.y(), J.z());
        m.rotate(beta / M_PI * 180.f, 0.f, 0.f, 1.f);
        m.rotate(90.f, 0.f, 1.f, 0.f);
        m.translate(0.f, 0.f, hj_norm/2);
        dessiner_cylindre(m, HJ);
    }

    // Axe 3
    matrix.translate(0.f, 0.f, 0.4f);
    {
        auto m = matrix;
        m.translate(J);
        dessiner_cylindre(m, axe3);
    }

    // Roue 4
    matrix.translate(0.f, 0.f, 0.4f);
    {
        auto m = matrix;
        m.translate(J);
        dessiner_cylindre(m, roue4);
    }

    // Cylindre J - K
    {
        auto m = matrix;
        m.translate(J.x()-hj_norm, J.y(), J.z());
        m.rotate(90.f, 0.f, 1.f, 0.f);
        m.translate(0.f, 0.f, hj_norm/2);
        dessiner_cylindre(m, JK);
    }

    // Piston
    {
        auto m = matrix;
        m.translate(K.x(), K.y(), K.z());
        m.rotate(90.f, 0.f, 1.f, 0.f);
        dessiner_cylindre(m, piston);
    }

    m_program->release();
}

void GLArea::keyPressEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();

    switch(ev->key()) {
        case Qt::Key_Space :
            m_angle += 1;
            if (m_angle >= 360) m_angle -= 360;
            update();
            break;
        case Qt::Key_A :
            if (m_timer->isActive())
                m_timer->stop();
            else m_timer->start();
            break;
        case Qt::Key_R :
            if (ev->text() == "r")
                 setRadius(m_radius-0.05);
            else setRadius(m_radius+0.05);
            break;
    }
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}

void GLArea::mousePressEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y();
}

void GLArea::dessiner_cylindre(const QMatrix4x4 &matrix, Cylindre &cylindre)
{
    cylindre.draw(matrix, m_program, m_posAttr, m_colAttr, m_matrixUniform);
}

void GLArea::onTimeout()
{
    m_anim += 0.01;
    if (m_anim > 1) m_anim = 0;
    update();
}

void GLArea::setRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    if (radius != m_radius && radius > 0.01 && radius <= 10) {
        m_radius = radius;
        qDebug() << "  emit radiusChanged()";
        emit radiusChanged(radius);
        update();
    }
}

Cylindre::Cylindre(float ep_cyl, float r_cyl, int nb_face, float coul_r, float coul_v, float coul_b)
    : ep_cyl(ep_cyl), r_cyl(r_cyl), nb_face(nb_face), coul_r(coul_r), coul_v(coul_v), coul_b(coul_b)
{}

void Cylindre::makeGLObjects()
{
    float angleR = (2.f * M_PI) / nb_face;
    float angleD = 360.f/nb_face;

    QVector3D A(0.f,0.f,ep_cyl/2.f),
            B(0.f,0.f,-ep_cyl/2.f),
            C(r_cyl, 0.f, A.z()),
            D(r_cyl, 0.f, B.z()),
            E(std::cos(angleR) * r_cyl, std::sin(angleR) * r_cyl, B.z()),
            F(std::cos(angleR) * r_cyl, std::sin(angleR) * r_cyl, A.z());

    QVector<GLfloat> vertData;
    vertData.reserve(nb_face*(6*(3+3)+4*(3+3)));

    auto pushV = [&vertData](const QVector3D& v) {
        for (int i = 0; i < 3; ++i) {
            vertData << v[i];
        }
    };

    auto pushC = [&vertData](float r, float g, float b) {
        vertData << r << g << b;
    };

    QMatrix4x4 matrix;
    for (int face = 0; face < nb_face; ++face) {

        pushV(matrix * A); pushC(coul_r, coul_v, coul_b);
        pushV(matrix * C); pushC(coul_r, coul_v, coul_b);
        pushV(matrix * F); pushC(coul_r, coul_v, coul_b);
        pushV(matrix * B); pushC(coul_r, coul_v, coul_b);
        pushV(matrix * D); pushC(coul_r, coul_v, coul_b);
        pushV(matrix * E); pushC(coul_r, coul_v, coul_b);

        matrix.rotate(angleD, 0.f, 0.f, 1.f);
    }

    matrix.setToIdentity();
    for (int face = 0; face < nb_face; ++face) {

        pushV(matrix * C); pushC(coul_r*0.8f, coul_v*0.8f, coul_b*0.8f);
        pushV(matrix * D); pushC(coul_r*0.8f, coul_v*0.8f, coul_b*0.8f);
        pushV(matrix * E); pushC(coul_r*0.8f, coul_v*0.8f, coul_b*0.8f);
        pushV(matrix * F); pushC(coul_r*0.8f, coul_v*0.8f, coul_b*0.8f);

        matrix.rotate(angleD, 0.f, 0.f, 1.f);
    }

    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(), vertData.count() * sizeof(GLfloat));
    m_vbo.release();
}

void Cylindre::tearGLObjects()
{
    m_vbo.destroy();
}

void Cylindre::draw(QMatrix4x4 matrix, QOpenGLShaderProgram* m_program, int m_posAttr, int m_colAttr, int m_matrixUniform)
{
    initializeOpenGLFunctions();
    m_vbo.bind();

    m_program->setUniformValue(m_matrixUniform, matrix);

    m_program->setAttributeBuffer(m_posAttr, GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
    m_program->setAttributeBuffer(m_colAttr, GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));

    m_program->enableAttributeArray(m_posAttr);
    m_program->enableAttributeArray(m_colAttr);

    glDrawArrays(GL_TRIANGLES, 0, nb_face * 6);
    glDrawArrays(GL_QUADS, nb_face * 6, nb_face * 4);

    m_program->disableAttributeArray(m_posAttr);
    m_program->disableAttributeArray(m_colAttr);
    m_vbo.release();
}
