// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef GLAREA_H
#define GLAREA_H

#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>

class GLArea : public QOpenGLWidget,
               protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit GLArea(QWidget *parent = 0);
    ~GLArea();

public slots:
    void setRadius(double radius);

signals:  // On ne les implémente pas, elles seront générées par MOC ;
          // les paramètres seront passés aux slots connectés.
    void radiusChanged(double newRadius);

protected slots:
    void onTimeout();

protected:
    void initializeGL() override;
    void doProjection();
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;

    void dessiner_cylindre_at(QVector3D pos, float angle, float ep_cyl, float r_cyl, int nb_face, float coul_r, float coul_v, float coul_b);
    void dessiner_cylindre(float ep_cyl, float r_cyl, int nb_face, float coul_r, float coul_v, float coul_b);

private:
    double m_angle = 0;
    QTimer *m_timer = nullptr;
    float m_alpha = 0;
    double m_radius = 0.5;
    double m_ratio = 1;
};

#endif // GLAREA_H
