// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"
#include <GL/glu.h>
#include <QDebug>
#include <QSurfaceFormat>
#include <QVector3D>
#include <cmath>

#define M_PI 3.14159265358979323846f

GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug() << "init GLArea" ;

    // Ce n'est pas indispensable
    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    m_timer = new QTimer(this);
    m_timer->setInterval(16);  // msec
    connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    connect (this, SIGNAL(radiusChanged(double)), this, SLOT(setRadius(double)));
}

GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    delete m_timer;

    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();

    // ici destructions de ressources GL

    doneCurrent();
}


void GLArea::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.f, 1.f, 1.f, 1.f);
}

void GLArea::doProjection()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    GLfloat hr = m_radius, wr = hr * m_ratio;
    //glFrustum(-wr, wr, -hr, hr, 1.0, 5.0);
    glOrtho (-wr, wr, -hr, hr, -2.0, 10.0);
    glMatrixMode(GL_MODELVIEW);
}

void GLArea::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;

    // C'est fait par défaut
    glViewport(0, 0, w, h);

    m_ratio = (double) w / h;
    doProjection();
}

void GLArea::paintGL()
{
    qDebug() << __FUNCTION__ ;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    gluLookAt (0, 0, 3.0, 0, 0, 0, 0, 1, 0);

    glRotatef (m_angle, 0, 1.0, 0.1f);

    float gh_norm = 1.f;
    float hj_norm = 2.5f;

    float alphaR = m_alpha * (2.f * M_PI);
    float alphaD = m_alpha * 360.f;

    QVector3D G(1.f, 0.f, 0.f), K(-3.5f, 0.f, 0.f);
    QVector3D O = G + QVector3D(0.f, 0.f, -0.25f);
    QVector3D H = G + QVector3D(std::cos(alphaR), std::sin(alphaR), 0.f) * gh_norm;
    QVector3D I(H.x(), G.y(), 0.f);
    QVector3D J(I.x() - std::sqrt(std::pow(hj_norm, 2) - std::pow(gh_norm * std::sin(alphaR), 2)), I.y(), 0.f);

    float beta = std::atan(H.y() / std::abs(J.x() - H.x()));

    // Roue 1 (principale)
    dessiner_cylindre_at(O, alphaD, 0.5f, gh_norm + 0.2f, 20, 0.5f, 0.5f, 1.f);

    // Axe 1
    glPushMatrix();
    glTranslatef(0.f, 0.f, -0.5f);
    dessiner_cylindre_at(G, alphaD, 1.1f, 0.1f, 20, 0.0f, 0.0f, 1.f);
    glPopMatrix();

    // Axe 2
    dessiner_cylindre_at(H, 0.f, 1.3f, 0.1f, 20, 0.0f, 0.0f, 1.f);

    // Roue 2
    glTranslatef(0.f, 0.f, 0.4f);
    dessiner_cylindre_at(H, 0.f, 0.4f, 0.3f, 20, 1.f, 0.5f, 0.5f);

    // Roue 3
    dessiner_cylindre_at(J, 0, 0.5f, 0.4f, 20, 1.f, 0.5f, 0.5f);

    // Cylindre H - J
    glPushMatrix();
    glTranslatef(J.x(), J.y(), J.z());
    glRotatef(beta / M_PI * 180.f, 0.f, 0.f, 1.f);
    glRotatef(90.f, 0.f, 1.f, 0.f);
    glTranslatef(0.f, 0.f, hj_norm/2);
    dessiner_cylindre(hj_norm, 0.1f, 20, 0.f, 1.f, 0.f);
    glPopMatrix();

    // Axe 3
    glTranslatef(0.f, 0.f, 0.4f);
    dessiner_cylindre_at(J, 0.f, 1.4f, 0.1f, 20, 0.0f, 0.0f, 1.f);

    // Roue 4
    glTranslatef(0.f, 0.f, 0.4f);
    dessiner_cylindre_at(J, 0.f, 0.5f, 0.4f, 20, 1.f, 0.5f, 0.5f);

    // Cylindre J - K
    glPushMatrix();
    glTranslatef(J.x()-hj_norm, J.y(), J.z());
    glRotatef(90.f, 0.f, 1.f, 0.f);
    glTranslatef(0.f, 0.f, hj_norm/2);
    dessiner_cylindre(hj_norm, 0.1f, 20, 0.f, 1.f, 0.f);
    glPopMatrix();

    // Piston
    glPushMatrix();
    glTranslatef(K.x(), K.y(), K.z());
    glRotatef(90.f, 0.f, 1.f, 0.f);
    dessiner_cylindre(1.f, 0.4f, 20, 1.f, 0.f, 0.2f);
    glPopMatrix();
    // on ne met pas glutSwapBuffers(); !
}

void GLArea::keyPressEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();

    switch(ev->key()) {
        case Qt::Key_Space :
            m_angle += 1;
            if (m_angle >= 360) m_angle -= 360;
            update();
            break;
        case Qt::Key_A :
            if (m_timer->isActive())
                m_timer->stop();
            else m_timer->start();
            break;
        case Qt::Key_R :
            if (ev->text() == "r")
                 setRadius(m_radius-0.05);
            else setRadius(m_radius+0.05);
            break;
    }
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}

void GLArea::mousePressEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y();
}

void GLArea::dessiner_cylindre_at(QVector3D pos, float angle, float ep_cyl, float r_cyl, int nb_face, float coul_r, float coul_v, float coul_b)
{
    glPushMatrix();
    glTranslatef(pos.x(), pos.y(), pos.z());
    glRotatef(angle, 0.f, 0.f, 1.f);
    dessiner_cylindre(ep_cyl, r_cyl, nb_face, coul_r, coul_v, coul_b);
    glPopMatrix();
}

void GLArea::dessiner_cylindre(float ep_cyl, float r_cyl, int nb_face, float coul_r, float coul_v, float coul_b)
{
    QVector3D A(0.f,0.f,ep_cyl/2.f), B(0.f,0.f,-ep_cyl/2.f);
    float angleR = (2.f * M_PI) / nb_face;
    float angleD = 360.f/nb_face;

    QVector3D C(r_cyl, 0.f, A.z()), F(std::cos(angleR) * r_cyl, std::sin(angleR) * r_cyl, A.z()),
              D(r_cyl, 0.f, B.z()), E(std::cos(angleR) * r_cyl, std::sin(angleR) * r_cyl, B.z());

    glPushMatrix();
    for (int face = 0; face < nb_face; ++face) {

        glColor3f(coul_r, coul_v, coul_b);
        glBegin(GL_TRIANGLES);
        glVertex3f(A.x(), A.y(), A.z());
        glVertex3f(C.x(), C.y(), C.z());
        glVertex3f(F.x(), F.y(), F.z());

        glVertex3f(B.x(), B.y(), B.z());
        glVertex3f(D.x(), D.y(), D.z());
        glVertex3f(E.x(), E.y(), E.z());
        glEnd();

        glColor3f(coul_r*0.8f, coul_v*0.8f, coul_b*0.8f);
        glBegin(GL_QUADS);
        glVertex3f(C.x(), C.y(), C.z());
        glVertex3f(D.x(), D.y(), D.z());
        glVertex3f(E.x(), E.y(), E.z());
        glVertex3f(F.x(), F.y(), F.z());
        glEnd();

        glRotatef(angleD, 0.f, 0.f, 1.f);
    }
    glPopMatrix();
}

void GLArea::onTimeout()
{
    qDebug() << __FUNCTION__ ;
    m_alpha += 0.001;
    if (m_alpha > 1) m_alpha = 0;
    update();
}

void GLArea::setRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    if (radius != m_radius && radius > 0.01 && radius <= 10) {
        m_radius = radius;
        qDebug() << "  emit radiusChanged()";
        emit radiusChanged(radius);
        makeCurrent();
        doProjection();
        doneCurrent();
        update();
    }
}




