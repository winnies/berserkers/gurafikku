#include <stdio.h>
#include <GL/glut.h>
#include <math.h>

#define M_PI 3.14159265358979323846

int anim_angle = 0;
unsigned int anim_delay = 50; // ms
int anim_flag = 0;
float aspect_ratio = 1.0;
int depth_flag = 1;
int flag_fill = 1;

int angle_anim = 0;
float cam_z, cam_hr, cam_near, cam_far;
enum { P_ORTHO, P_FRUSTUM, P_MAX } cam_proj;

//Variables roue
int nb_dents;
float r_trou;
float r_roue;
float h_dent;
float coul_r, coul_v, coul_b;
float ep_roue;

void init_roue_1() {
    nb_dents = 10;
    r_trou = 0.5;
    r_roue = 0.8;
    h_dent = 0.3;
    coul_r = 1, coul_v = 0, coul_b = 0;
    ep_roue = 0.3;
}

void init_roue_2() {
    nb_dents = 10;
    r_trou = 0.6;
    r_roue = 0.8;
    h_dent = 0.3;
    coul_r = 0, coul_v = 1, coul_b = 0;
    ep_roue = 0.5;
}

void init_roue_3() {
    nb_dents = 20;
    r_trou = 1;
    r_roue = 1.6;
    h_dent = 0.3;
    coul_r = 0, coul_v = 0, coul_b = 1;
    ep_roue = 0.6;
}

void cam_init ()
{
    cam_z = 3; cam_hr = 1; cam_near = 1; cam_far = 5; cam_proj = P_ORTHO;
}

void do_projection (void)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    float wr = cam_hr * aspect_ratio;
    switch (cam_proj) {
        case P_ORTHO :
            printf ("glOrtho (%.1f, %.1f, %.1f, %.1f, %.1f, %.1f)  cam_z = %.1f\n",
                -wr, wr, -cam_hr, cam_hr, cam_near, cam_far, cam_z);
            glOrtho (-wr, wr, -cam_hr, cam_hr, cam_near, cam_far);
            break;
        case P_FRUSTUM :
            printf ("Frustum (%.1f, %.1f, %.1f, %.1f, %.1f, %.1f)  cam_z = %.1f\n",
                -wr, wr, -cam_hr, cam_hr, cam_near, cam_far, cam_z);
            glFrustum (-wr, wr, -cam_hr, cam_hr, cam_near, cam_far);
            break;
        default :;
    }

    glMatrixMode(GL_MODELVIEW);
}

void on_reshape_win(int width, int height)
{
    printf ("Reshape %d %d\n", width, height);
    glViewport(0, 0, width, height);
    aspect_ratio = (float) width / height;

    do_projection();
}

void dessiner_bloc_dent(void) {

    float angle = (2 * M_PI) / nb_dents;
    float bas_dent = r_roue - h_dent / 2 ;
    float haut_dent = r_roue + h_dent / 2 ;

    if(flag_fill)
        glBegin(GL_POLYGON);
    else
        glBegin(GL_LINE_LOOP);

    glColor3f (coul_r, coul_v, coul_b);
    glVertex3f(r_trou*cos(angle/2), r_trou*sin(angle/2), 0); // H
    glVertex3f(r_trou, 0, 0); // A
    glVertex3f(bas_dent, 0, 0); // B
    glVertex3f(bas_dent*cos(angle/4), bas_dent*sin(angle/4), 0); // C
    glVertex3f(haut_dent*cos(angle/2), haut_dent*sin(angle/2), 0); // D
    glVertex3f(haut_dent*cos(3*angle/4), haut_dent*sin(3*angle/4), 0); // E
    glVertex3f(bas_dent*cos(angle), bas_dent*sin(angle), 0); // F
    glVertex3f(r_trou*cos(angle), r_trou*sin(angle), 0); // G
    glEnd();
}

void dessiner_facettes_bloc() {
    if(!flag_fill)
        return;

    float angle = (2 * M_PI) / nb_dents;
    float bas_dent = r_roue - h_dent / 2 ;
    float haut_dent = r_roue + h_dent / 2 ;
    float demi_ep = ep_roue / 2;

    glBegin(GL_QUADS);
    glColor3f (coul_r, coul_v, coul_b);

    glColor3f(coul_r*0.8, coul_v*0.8, coul_b*0.8);
    glVertex3f(r_trou, 0, demi_ep); // A
    glVertex3f(r_trou*cos(angle/2), r_trou*sin(angle/2), demi_ep); // H
    glVertex3f(r_trou*cos(angle/2), r_trou*sin(angle/2), -demi_ep); // H'
    glVertex3f(r_trou, 0, -demi_ep); // A'

    glVertex3f(r_trou*cos(angle/2), r_trou*sin(angle/2), demi_ep); // H
    glVertex3f(r_trou*cos(angle), r_trou*sin(angle), demi_ep); // G
    glVertex3f(r_trou*cos(angle), r_trou*sin(angle), -demi_ep); // G'
    glVertex3f(r_trou*cos(angle/2), r_trou*sin(angle/2), -demi_ep); // H'

    glColor3f(coul_r*0.6, coul_v*0.6, coul_b*0.6);
    glVertex3f(bas_dent, 0, demi_ep); // B
    glVertex3f(bas_dent*cos(angle/4), bas_dent*sin(angle/4), demi_ep); // C
    glVertex3f(bas_dent*cos(angle/4), bas_dent*sin(angle/4), -demi_ep); // C'
    glVertex3f(bas_dent, 0, -demi_ep); // B'

    glVertex3f(bas_dent*cos(angle/4), bas_dent*sin(angle/4), demi_ep); // C
    glVertex3f(haut_dent*cos(angle/2), haut_dent*sin(angle/2), demi_ep); // D
    glVertex3f(haut_dent*cos(angle/2), haut_dent*sin(angle/2), -demi_ep); // D'
    glVertex3f(bas_dent*cos(angle/4), bas_dent*sin(angle/4), -demi_ep); // C'

    glColor3f(coul_r*0.8, coul_v*0.8, coul_b*0.8);
    glVertex3f(haut_dent*cos(angle/2), haut_dent*sin(angle/2), demi_ep); // D
    glVertex3f(haut_dent*cos(3*angle/4), haut_dent*sin(3*angle/4), demi_ep); // E
    glVertex3f(haut_dent*cos(3*angle/4), haut_dent*sin(3*angle/4), -demi_ep); // E'
    glVertex3f(haut_dent*cos(angle/2), haut_dent*sin(angle/2), -demi_ep); // D'

    glColor3f(coul_r*0.6, coul_v*0.6, coul_b*0.6);
    glVertex3f(haut_dent*cos(3*angle/4), haut_dent*sin(3*angle/4), demi_ep); // E
    glVertex3f(bas_dent*cos(angle), bas_dent*sin(angle), demi_ep); // F
    glVertex3f(bas_dent*cos(angle), bas_dent*sin(angle), -demi_ep); // F'
    glVertex3f(haut_dent*cos(3*angle/4), haut_dent*sin(3*angle/4), -demi_ep); // E'

    glEnd();
}

void dessiner_cote_roue() {

    glPushMatrix();
    for(int i = 0; i < nb_dents; i++) {
        dessiner_bloc_dent();
        glRotatef (360 / nb_dents, 0, 0, 1);
    }
    glPopMatrix();
}

void dessiner_roue() {

    glPushMatrix();
    for(int i = 0; i < nb_dents; i++) {
        dessiner_facettes_bloc();
        glRotatef (360 / nb_dents, 0, 0, 1);
    }
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0, 0, ep_roue/2);
    dessiner_cote_roue();
    glTranslatef(0, 0, -ep_roue);
    dessiner_cote_roue();
    glPopMatrix();
}

void on_display_win(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    gluLookAt (0, 0, cam_z, 0, 0, 0, 0, 1, 0);
    
    glRotatef (anim_angle, 0, 1.0, 0.1);

    glColor3f (0, 0, 1);
    glutWireCube (2.0);

    glPushMatrix();
    glRotatef(angle_anim, 0, 0, 1);
    init_roue_1();
    dessiner_roue();
    glPopMatrix();

    glPushMatrix();
    init_roue_2();
    glTranslatef(1.7, 0, 0);
    glRotatef (360 / nb_dents / 4, 0, 0, 1);
    glRotatef(-angle_anim, 0, 0, 1);
    dessiner_roue();
    glPopMatrix();

    glPushMatrix();
    init_roue_3();
    glTranslatef(0, -2.48, 0);
    glRotatef (13.5, 0, 0, 1);
    glRotatef(-angle_anim/2, 0, 0, 1);
    dessiner_roue();
    glPopMatrix();

    glutSwapBuffers();
}

void on_timer (int id)
{
    if (!anim_flag) return;
    anim_angle += 1;
    if (anim_angle >= 360) anim_angle = 0;
    glutPostRedisplay();
    glutTimerFunc (anim_delay, on_timer, id);  // on relance le même timer
}

void change_valf (float *val, int negate, float incr, float min_val)
{
    *val += negate ? -incr : incr;
    if (*val <= min_val) *val = min_val;
}

void print_help ()
{
    printf ("h help  i init  a anim  p proj  zZ cam_z  rR radius  nN near  fF far  dD dist  b zbuffer q fill\n");
}

void on_key_pressed (unsigned char key, int x, int y)
{
    switch (key) {
        case 'i' :
            cam_init ();
            break;
        case 'a' :
            anim_flag = !anim_flag;
            if (anim_flag) glutTimerFunc (anim_delay, on_timer, 0);
            break;
        case 'p' : 
            cam_proj += 1;
            if (cam_proj >= P_MAX) cam_proj = 0;
            break;
        case 'z' : 
        case 'Z' :
            change_valf (&cam_z, key == 'z', 0.1, -100);
            break;
        case 'r' : 
        case 'R' :
            change_valf (&cam_hr, key == 'r', 0.1, 0.1);
            break;
        case 'n' : 
        case 'N' :
            change_valf (&cam_near, key == 'n', 0.1, 0.1);
            break;
        case 'f' : 
        case 'F' :
            change_valf (&cam_far, key == 'f', 0.1, 0.1);
            break;
        case 'd' : 
        case 'D' :
            change_valf (&cam_z, key == 'd', 0.1, -100);
            change_valf (&cam_near, key == 'd', 0.1, 0.1);
            change_valf (&cam_far, key == 'd', 0.1, 0.1);
            break;
        case 'b' :
            depth_flag = !depth_flag;
            printf ("depth_flag is %d\n", depth_flag);
            if (depth_flag) glEnable(GL_DEPTH_TEST);
            else glDisable(GL_DEPTH_TEST);
            break;
        case 'h' :
            print_help ();
            break;
        case 'q' :
            flag_fill = !flag_fill;
            printf ("fill_flag is %d\n", flag_fill);
            break;
        case 32 :
            angle_anim = (angle_anim + 2) % 360;
            printf("Rotation des roues de %d degrés\n", angle_anim);
            break;
        case 27 : // ESC
            exit(1);
    }

    do_projection();
    glutPostRedisplay();
}

int main(int argc, char **argv) 
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    cam_init();
    print_help();

    glutCreateWindow("Roues");
    glutReshapeFunc(on_reshape_win);
    glutDisplayFunc(on_display_win);
    glutKeyboardFunc(on_key_pressed);

    glEnable(GL_DEPTH_TEST); // Après glutCreateWindow qui crée un context GL

    glutMainLoop();
    return 0;
}